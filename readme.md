# deep-art


AI artist that paints artwork of the uploaded image. This new artwork looks like my previous oil paintings. It uses the Neural Style Transfer algorithm but the differ is it can use not only one pictures as style. If the user likes the resulting picture, then a high-quality image is generated for printing on canvas in a large size.


## Installation
**Requirements**

- python >=3.8, <=3.10
- poetry >= 1.1.6
- docker
- docker-compose

**How to start**
- clone project

- create virtual enviroment for this project

- install libraries:

    ```
    poetry install
    ```
- run container with rabbitmq:
    ```
    docker-compose up
    ```
- run application:
  ```
  python web/app.py
  ```
___
Now you can work with the server by sending REST API requests:
1. send POST request `create_artwork` with file of content 
1. The server should reply with your `request_id`
1. Send GET request `get_status` with parameter request_id from the previous step. Example:
   
    ```http://127.0.0.1:5000/get_status?request_id=80d19c4a-db9c-47dc-b3f8-b07a77bc7f42```
1. If the picture is ready you'll get it. Else you'll get message: `New picture isn't ready yet`, so you could try later

___

## My plans:
- Docker integration
- Unit tests coverage more than 85% of code
- CI/CD
- Telegram bot
- Better quality of pictures
- High resolution generated images for canvas printing
- A ML model that will choose the best set of pictures to handle the request


**This is only start, it will be developed and improved!**
