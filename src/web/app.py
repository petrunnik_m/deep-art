import dramatiq
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from flask import Flask

from web import handlers
from web.config import get_config
from web.deep_art_api import routes as deep_art_route
from web.logger import init_logger

config = get_config()
init_logger(config.LOGGER_LEVEL)

app = Flask(__name__)
app.config.from_object(config)

deep_art_route.init_routes(app)

# create rabbitmqBroker for dramatiq
rabbitmq_broker = RabbitmqBroker(host="127.0.0.1", confirm_delivery=True)
dramatiq.set_broker(rabbitmq_broker)

# add special handlers
app.errorhandler(404)(handlers.error_404)
app.errorhandler(Exception)(handlers.default_error_handler)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=config.DEBUG)
