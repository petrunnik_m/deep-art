import os
from pathlib import Path

from dotenv import load_dotenv


class BaseConfig:
    def __init__(self):
        self.BASE_DIR = Path(__file__).parent
        self.CURRENT_ENV = os.environ.get('CURRENT_ENV', 'dev')

        self.LOGGER_LEVEL = 'INFO'

        # flask
        self.DEBUG = False
        self.TESTING = False
        self.SECRET_KEY = 'erhjr34yhrrtj'
        self.UPLOAD_FOLDER = self.BASE_DIR / 'media' / 'in'
        self.RESULT_FOLDER = self.BASE_DIR / 'media' / 'out'


class DevConfig(BaseConfig):
    def __init__(self):
        super().__init__()

        self.LOGGER_LEVEL = 'DEBUG'

        # flask
        self.DEBUG = True
        self.TESTING = True


class TestConfig(DevConfig):
    def __init__(self):
        super().__init__()

        self.LOGGER_LEVEL = 'INFO'


class ProdConfig(BaseConfig):
    def __init__(self):
        super().__init__()


config = {'dev': DevConfig, 'test': TestConfig, 'prod': ProdConfig}


def _get_config_gen():
    load_dotenv()
    conf = config[os.environ.get('CURRENT_ENV', 'dev')]()
    while True:
        yield conf


_config_gen = _get_config_gen()
next(_config_gen)


def get_config():
    return next(_config_gen)
