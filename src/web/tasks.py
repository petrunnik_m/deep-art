from pathlib import Path

import dramatiq

from command.create_art import Painter


@dramatiq.actor
def create_artwork_task(source_image: str, output_image: str):
    Painter().draw(Path(source_image), Path(output_image))
