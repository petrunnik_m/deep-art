import uuid

from flask import Response, request, send_file

from web.config import get_config
from web.tasks import create_artwork_task

_IMAGE_SUFFIX = '.jpg'


def create_artwork():
    conf = get_config()
    input_files = request.files
    if input_files.__len__() > 1:
        return Response(response="There are several input files. Please, choose one", status=400)
    elif input_files.__len__() == 0:
        return Response(response="There isn't any file in request. Please, attache something", status=400)
    file = input_files['file']
    request_id = str(uuid.uuid4())
    input_file_path = (conf.UPLOAD_FOLDER / request_id).with_suffix(_IMAGE_SUFFIX)
    file.save(input_file_path)
    out_path = (conf.RESULT_FOLDER / request_id).with_suffix(_IMAGE_SUFFIX)
    create_artwork_task.send(source_image=str(input_file_path), output_image=str(out_path))
    return Response(response=f"Your request ID: {request_id}")


def get_status():
    conf = get_config()
    request_id = request.args.get("request_id")
    requested_file_path = (conf.RESULT_FOLDER / request_id).with_suffix(_IMAGE_SUFFIX)
    initial_file_path = (conf.UPLOAD_FOLDER / request_id).with_suffix(_IMAGE_SUFFIX)
    if not initial_file_path.exists():
        return Response(response="There isn't such request id", status=400)
    elif requested_file_path.exists():
        return send_file(requested_file_path, as_attachment=True)
    else:
        return Response(response="New picture isn't ready yet", status=200)
