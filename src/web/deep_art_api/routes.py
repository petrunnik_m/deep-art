from __future__ import annotations

from typing import TYPE_CHECKING

from web.deep_art_api import views

if TYPE_CHECKING:
    from flask import Flask


def init_routes(app: Flask):
    app.route('/create_artwork', methods=['POST'])(views.create_artwork)
    app.route('/get_status', methods=['GET'])(views.get_status)
