import logging


def error_404(error):
    return '404 error'


def default_error_handler(error):
    logging.getLogger().exception('Возникла непредвиденная ошибка при обработке запроса')
    return 'Возникла непредвиденная ошибка при обработке запроса'
