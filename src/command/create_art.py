import time
from pathlib import Path

import numpy as np
import tensorflow as tf
from PIL import Image

from command.model_helper import (
    STYLE_LAYERS,
    get_image_center,
    get_layer_outputs,
    tensor_to_image,
    train_step,
)

BASE_DIR = Path(__file__).absolute().parent

IMG_SIZE = 400
N_CHANEL = 3
LEARNING_RATE = 0.001
EPOCHS = 2501

STYLE_DIR_PATH = BASE_DIR / 'style'

CONTENT_LAYER = [('block5_conv2', 1)]
vgg = tf.keras.applications.VGG19(
    include_top=False,
    input_shape=(IMG_SIZE, IMG_SIZE, N_CHANEL),
    weights=BASE_DIR / 'model_files' / 'vgg19_weights_tf_dim_ordering_tf_kernels_notop.h5',
)
vgg.trainable = False
VGG_MODEL_OUTPUTS = get_layer_outputs(vgg, STYLE_LAYERS + CONTENT_LAYER)


def preprocess_style():
    style_images = []
    for style_path in Path(STYLE_DIR_PATH).glob('*.jpg'):
        style_image = np.array(get_image_center(Image.open(style_path)).resize((IMG_SIZE, IMG_SIZE)))
        style_image = tf.constant(np.reshape(style_image, ((1,) + style_image.shape)))
        style_images.append(style_image)

    # Assign the input of the model to be the "style" image
    preprocessed_styles = [
        tf.Variable(tf.image.convert_image_dtype(style_image, tf.float32)) for style_image in style_images
    ]
    a_S_list = [VGG_MODEL_OUTPUTS(preprocessed_style) for preprocessed_style in preprocessed_styles]
    return a_S_list


def run_create_artwork(content_path, result_path, a_S_list):
    content_image = np.array(get_image_center(Image.open(content_path)).resize((IMG_SIZE, IMG_SIZE)))
    content_image = tf.constant(np.reshape(content_image, ((1,) + content_image.shape)))

    generated_image = tf.Variable(tf.image.convert_image_dtype(content_image, tf.float32))
    noise = tf.random.uniform(tf.shape(generated_image), -0.25, 0.25)
    generated_image = tf.add(generated_image, noise)
    generated_image = tf.clip_by_value(generated_image, clip_value_min=0.0, clip_value_max=1.0)

    preprocessed_content = tf.Variable(tf.image.convert_image_dtype(content_image, tf.float32))
    a_C = VGG_MODEL_OUTPUTS(preprocessed_content)

    optimizer = tf.keras.optimizers.Adam(learning_rate=LEARNING_RATE)

    generated_image = tf.Variable(generated_image)

    start_time = time.time()
    for i in range(EPOCHS):
        train_step(generated_image, VGG_MODEL_OUTPUTS, optimizer, a_C, a_S_list)
        if i % 100 == 0:
            print(f"Epoch {i} ")
    image = tensor_to_image(generated_image)
    image.save(result_path)
    print(f'Painting took {round(time.time() - start_time)} seconds')


class Painter:
    _a_s_list = None

    def draw(self, source_image: Path, output_image: Path):
        if self._a_s_list is None:
            Painter._a_s_list = preprocess_style()

        run_create_artwork(source_image, output_image, self._a_s_list)
